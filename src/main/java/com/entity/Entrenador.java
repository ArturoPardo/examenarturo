package com.entity;

import java.io.Serializable;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Entrenador extends Persona implements Serializable{

	private static final long serialVersionUID = -1680948027052366045L;

}
