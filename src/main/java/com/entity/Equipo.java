package com.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Equipo {

	
	private Long id;
	private String nombre;
	private int anyoFundacion;
	private Entrenador entrenador;
	

	@ManyToMany
	@JoinTable(name = "equipo_jugador", joinColumns = @JoinColumn(name = "id_equipo"), inverseJoinColumns = @JoinColumn(name = "id_jugador"))
	List<Jugador> jugador;
	

}
