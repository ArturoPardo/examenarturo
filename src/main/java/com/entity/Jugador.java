package com.entity;

import java.io.Serializable;

import javax.persistence.Entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Jugador extends Persona implements Serializable{

	private static final long serialVersionUID = 7293296158958941192L;
	private Equipo equipo;

}
