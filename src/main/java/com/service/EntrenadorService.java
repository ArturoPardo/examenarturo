package com.service;

import java.util.List;

import com.entity.Entrenador;

public interface EntrenadorService {
	List<Entrenador> getAllEntrenador();

	Entrenador saveOrUpdateEntrenador(Entrenador entrenador);

	void deleteEntrenador(Long idEntrenador);

	Entrenador findeOneEntrenador(Long idEntrenador);

}
