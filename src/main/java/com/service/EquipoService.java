package com.service;

import java.util.List;

import com.entity.Equipo;
import com.entity.Jugador;

public interface EquipoService {
	List<Equipo> getAllEquipo();

	Equipo saveOrUpdateEquipo(Equipo a);

	void deleteEquipo(Long id);

	Equipo findeOneEquipo(Long id);
	
	List<Jugador> getAsignaturasJugador(Long idJugador);


}
