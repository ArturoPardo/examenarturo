package com.service;

import java.util.List;

import com.entity.Equipo;
import com.entity.Jugador;

public interface JugadorService {
	
	List<Jugador> getAllJugador();

	Jugador findeOneJugador(Long idJugador);

	Jugador saveOrUpdateJugador(Jugador jugador);

	void deleteJugador(Long idJugador);
	
	List<Equipo> getJugadorEquipo(Long idJugador);

}
