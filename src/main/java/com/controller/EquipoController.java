package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Equipo;
import com.service.EquipoService;

@RestController
@RequestMapping("/api/equipo/")
public class EquipoController {
	@Autowired
	private EquipoService EquipoService;
	
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE} )
	public List<Equipo> getAll() {
		return this.EquipoService.getAllEquipo();
	}
	
	@GetMapping("/{id}")
	public Equipo getEquipo(@PathVariable("id") long id) {
		return this.EquipoService.findeOneEquipo(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Equipo addEquipo(@RequestBody final Equipo Equipo) {
		return this.EquipoService.saveOrUpdateEquipo(Equipo);
	}
}

