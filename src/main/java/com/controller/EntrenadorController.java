package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Entrenador;
import com.service.EntrenadorService;


@RestController
@RequestMapping("/api/entrenador/")
public class EntrenadorController {
	@Autowired
	private EntrenadorService entrenadorService;
	
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE} )
	public List<Entrenador> getAll() {
		return this.entrenadorService.getAllEntrenador();
	}
	
	@GetMapping("/{id}")
	public Entrenador getentrenador(@PathVariable("id") long id) {
		return this.entrenadorService.findeOneEntrenador(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Entrenador addentrenador(@RequestBody final Entrenador entrenador) {
		return this.entrenadorService.saveOrUpdateEntrenador(entrenador);
	}
}
