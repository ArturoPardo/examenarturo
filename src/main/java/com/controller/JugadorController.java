package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Jugador;
import com.service.JugadorService;

@RestController
@RequestMapping("/api/jugador/")
public class JugadorController {
	@Autowired
	private JugadorService jugadorService;
	
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE} )
	public List<Jugador> getAll() {
		return this.jugadorService.getAllJugador();
	}
	
	@GetMapping("/{id}")
	public Jugador getJugador(@PathVariable("id") long id) {
		return this.jugadorService.findeOneJugador(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Jugador addJugador(@RequestBody final Jugador jugador) {
		return this.jugadorService.saveOrUpdateJugador(jugador);
	}
}
